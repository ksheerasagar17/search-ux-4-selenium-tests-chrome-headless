using System;
using NUnit.Framework;
using OpenQA.Selenium;
using SearchUx.Selenium;
using System.Threading;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Linq;

namespace SeleniumTests
{
    public class SearchUxTests : IDisposable
    {
        private IWebDriver _driver;
        private WebDriverWait _webDriverWait;

        public SearchUxTests()
        {
            Console.WriteLine("SearchUxTests Constructor ");
        }

        [SetUp]
        public void Init()
        {
            Console.WriteLine($"Is OsPlatform Linux - {RuntimeInformation.IsOSPlatform(OSPlatform.Linux)}");
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--headless");
            chromeOptions.AddArgument("--disable-gpu");
            chromeOptions.AddArgument("--no-sandbox");
            //chromeOptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
            _driver = new ChromeDriver(chromeOptions);
            _webDriverWait = new WebDriverWait(_driver, TimeSpan.FromMilliseconds(10000));
        }

        [Test]
        public void ClickingCardButton_ShouldShowResultsInCardView()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingCardButton_ShouldShowResultsInCardView.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("toggle-button")).Click();
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='list']")));
            //Revert back the toggle
            _driver.FindElement(By.Id("toggle-button")).Click();
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='card']")));
        }

        [Test]
        public void SearchForDealKeyWord_ShouldRedirectToPilotSearch()
        {
            var searchHomePageURl = $"{Environment.GetEnvironmentVariable("UI_URL")}&q=deal";
            Console.WriteLine($"Navigating to {searchHomePageURl}.");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Assert.AreEqual(new Uri("http://pilot.search.dell.com").Host, new Uri(_driver.Url).Host);
        }

        [Test]
        public void SearchForDirectContentKeyWord_ShouldRedirectToPilotSearch()
        {
            var searchHomePageURl = $"{Environment.GetEnvironmentVariable("UI_URL")}&q=power more";
            Console.WriteLine($"Navigating to {searchHomePageURl}.");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Assert.AreEqual(new Uri("http://pilot.search.dell.com").Host, new Uri(_driver.Url).Host);
        }

        [Test]
        public void LwpCookieContainsEep_ShouldRedirectToPilotSearch()
        {
            Console.WriteLine($"Lwp cookie set to value eep.");
            var searchHomePageURl = $"{Environment.GetEnvironmentVariable("UI_URL")}&q=xps";
            Console.WriteLine($"Navigating to {searchHomePageURl}.");

            //Navtigating twice intentionally, not an accident or mistake.
            //Somehow Selenium needs this trick to set cookies properly.
            _driver.Navigate().GoToUrl(searchHomePageURl);
            var lwpCookie = new Cookie("lwp", "s=eep");
            _driver.Manage().Cookies.AddCookie(lwpCookie);
            _driver.Navigate().GoToUrl(searchHomePageURl);

            Assert.AreEqual(new Uri("http://pilot.search.dell.com").Host, new Uri(_driver.Url).Host);
            _driver.Manage().Cookies.DeleteCookie(lwpCookie);
            Console.WriteLine($"Deleted Lwp cookie with value eep.");
        }

        [Test]
        public void ClickingListButton_ShouldShowResultsInListView()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingCardButton_ShouldShowResultsInCardView.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("toggle-button")).Click();
            Assert.IsNotNull(_driver.FindElements(By.CssSelector(".coveo-list-layout")));
        }

        [Ignore("Skipped as known issue with list view")]
        [Test]
        public void ClickingListButton_ShouldStayInResultsInSelectedView_EvenIfSortOrderIsChanged()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingCardButton_ShouldShowResultsInCardView.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("toggle-button")).Click();
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='list']")));
            //click the sort by Drop down 
            _driver.FindElement(By.Id("SortDropdowndropdownMenuButton")).Click();
            //And then click the High to Low option            
            _driver.FindElement(By.Id("dropdown-item-Price-Low-to-High")).Click();
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='list']")));
        }

        [Ignore("Skipped as known issue with list view")]
        [Test]
        public void ClickingListButton_ShouldStayInResultsInSelectedView_WhenNewQuery()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingCardButton_ShouldShowResultsInCardView.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("toggle-button")).Click();
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='list']")));
            //Search Again
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("xps");
            //And then click the High to Low option            
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='list']")));
        }

        [Ignore("Skipped as known issue with list view")]
        [Test]
        public void ClickingListButton_ShouldStayInResultsInSelectedView_WhenRefined()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingCardButton_ShouldShowResultsInCardView.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("toggle-button")).Click();
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='list']")));
            RetryFindAndClick(By.Id("For-Home-radio-button"));
            //And then click the High to Low option            
            Assert.IsNotNull(_driver.FindElement(By.CssSelector(".CoveoResultList:not(.coveo-hidden)[data-layout='list']")));
        }

        [Test]
        public void ClickingListButton_For_SNP_should_ShowTechSpec()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            string expectedMfrPartText = "Manufacturer Part ";
            string expectedDellPartText = "Dell Part ";
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[snp]";

            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingListButton_For_SNP_should_ShowTechSpec.png", ScreenshotImageFormat.Png);
            RetryFindAndClick(By.Id("toggle-button"), 0, 5);

            //div.coveo-list-layout:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > span:nth-child(1)
            var techSpecMfr = RetryFind(By.Id("tech-spec-0-Manufacturer-Part-#"));
            var snpTechMfrPartNumber = techSpecMfr.GetAttribute("innerText");

            //div.coveo-list-layout:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(1) > li:nth-child(2) > span:nth-child(1)
            snpTechMfrPartNumber = snpTechMfrPartNumber.Substring(0, snpTechMfrPartNumber.IndexOf("#"));
            var techSpecDell = RetryFind(By.Id("tech-spec-0-Dell-Part-#"));
            var snpTechDellPartNumber = techSpecDell.GetAttribute("innerText");
            snpTechDellPartNumber = snpTechDellPartNumber.Substring(0, snpTechDellPartNumber.IndexOf("#"));

            Assert.IsNotNull(_driver.FindElements(By.CssSelector(".coveo-card-layout")));
            Assert.AreEqual(expectedMfrPartText, snpTechMfrPartNumber);
            Assert.AreEqual(expectedDellPartText, snpTechDellPartNumber);
        }

        [Test]
        public void ClickingListButton_For_Systems_should_ShowTechSpecWithPrice()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            string expectedMfrPartText = "Dell Price";
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingListButton_For_Systems_should_ShowTechSpecWithPrice.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("toggle-button")).Click();
            var techSpec = RetryFind(By.Id("dell-price-0"));
            var techSpecDellPrice = techSpec.GetAttribute("innerText");
            Assert.IsTrue(techSpecDellPrice.Contains(expectedMfrPartText));
            //Revert the toggle back to Card view
            _driver.FindElement(By.Id("toggle-button")).Click();
        }

        [Test]
        public void ClickingSortButton_HighToLow_For_Systems_should_ShowSystemsInDescendingOrder()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingListButton_For_Systems_should_ShowTechSpecWithPrice.png", ScreenshotImageFormat.Png);
            //Toggel to card view
            _driver.FindElement(By.Id("toggle-button")).Click();
            //click the sort by Drop down 
            _driver.FindElement(By.Id("SortDropdowndropdownMenuButton")).Click();
            //And then click the High to Low option
            _driver.FindElement(By.Id("dropdown-item-Price-High-to-Low")).Click();
            Thread.Sleep(4000);
            var priceFirstel = RetryFind(By.Id("price-0"));
            var priceSecondel = RetryFind(By.Id("price-1"));
            Regex digitsOnly = new Regex(@"[^\d\.]");
            string priceFirst = digitsOnly.Replace(priceFirstel.Text, "");
            string priceSecond = digitsOnly.Replace(priceSecondel.Text, "");
            Assert.NotNull(priceFirst);
            Assert.NotNull(priceSecond);
            bool isFirstPriceHigher = double.Parse(priceFirst) >= double.Parse(priceSecond);
            Assert.IsTrue(isFirstPriceHigher);
            //revert the toggle back
            _driver.FindElement(By.Id("toggle-button")).Click();
        }

        [Test]
        public void ClickingSortButton_LowToHigh_For_Systems_should_ShowSystemsInDescendingOrder()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingListButton_For_Systems_should_ShowTechSpecWithPrice.png", ScreenshotImageFormat.Png);
            //Toggel to card view
            _driver.FindElement(By.Id("toggle-button")).Click();
            //click the sort by Drop down 
            _driver.FindElement(By.Id("SortDropdowndropdownMenuButton")).Click();
            //And then click the High to Low option            
            _driver.FindElement(By.Id("dropdown-item-Price-Low-to-High")).Click();
            Thread.Sleep(4000);
            var priceFirstel = RetryFind(By.Id("price-0"));
            var priceSecondel = RetryFind(By.Id("price-1"));
            Regex digitsOnly = new Regex(@"[^\d\.]");
            string priceFirst = digitsOnly.Replace(priceFirstel.Text, "");
            string priceSecond = digitsOnly.Replace(priceSecondel.Text, "");
            Assert.NotNull(priceFirst);
            Assert.NotNull(priceSecond);
            bool isFirstPriceLower = double.Parse(priceFirst) <= double.Parse(priceSecond);
            Assert.IsTrue(isFirstPriceLower);
            //revert the toggle back
            _driver.FindElement(By.Id("toggle-button")).Click();
        }

        [Test]
        public void Clicking_DHS_Systems_AddToCart_ShouldShowGoToCart()
        {
            var searchHomePageURl = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("ClickingAddToCart_ShouldShowGoToCart.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Saved screenshot");
            Console.WriteLine($"Clicking segment radio button");
            RetryFindAndClick(By.Id("For-Home-radio-button"));
            RetryFindAndClick(By.Id("add-to-cart-0"));
            Console.WriteLine($"Clicked on add to cart button ");
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("shop/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);     
        }

        [Test]
        public void Clicking_BSD_Systems_AddToCart_ShouldShowGoToCart()
        {
            var searchHomePageURl = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=bsd&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("ClickingAddToCart_ShouldShowGoToCart.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Saved screenshot");
            Console.WriteLine($"Clicking segment radio button");
            RetryFindAndClick(By.Id("For-Work-radio-button"));
            RetryFindAndClick(By.Id("add-to-cart-0"));
            Console.WriteLine($"Clicked on add to cart button ");
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("work/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);
        }

        [Test]
        public void Clicking_DHS_SnP_AddToCart_ShouldShowGoToCart()
        {
            var searchHomePageURl = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[snp]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("ClickingAddToCart_ShouldShowGoToCart.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Saved screenshot");
            Console.WriteLine($"Clicking segment radio button");
            RetryFindAndClick(By.Id("For-Home-radio-button"));
            RetryFindAndClick(By.Id("add-to-cart-0"));
            Console.WriteLine($"Clicked on add to cart button ");
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("shop/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);
        }

        [Test]
        public void Clicking_BSD_SnP_AddToCart_ShouldShowGoToCart()
        {
            var searchHomePageURl = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=bsd&f:@dpproducttype=[snp]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("ClickingAddToCart_ShouldShowGoToCart.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Saved screenshot");
            Console.WriteLine($"Clicking segment radio button");
            RetryFindAndClick(By.Id("For-Work-radio-button"));
            Console.WriteLine($"Clicked segment radio button to choose bsd segment");
            RetryFindAndClick(By.Id("add-to-cart-0"));
            Console.WriteLine($"Clicked on add to cart button ");
            //Cart takes too long to load.  Adding sleep time to for the time being
            Thread.Sleep(9000);
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("work/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);
        }

        [Test]
        public void Clicking_BSD_SNP_Customize_ShouldGotoCustomize()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=bsd&f:@dpproducttype=[snp]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("Clicking_BSD_SNP_Customize_ShouldGotoCustomize.png", ScreenshotImageFormat.Png);
            //Click the BSD radio button
            RetryFindAndClick(By.Id("For-Work-radio-button"));
            Console.WriteLine($"Clicking Customize button");
            RetryFindAndClick(By.Id("customize-0"));
            Console.WriteLine($"Clicked on customize button ");
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url for Clicking_BSD_SNP_Customize_ShouldGotoCustomize is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("shop/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);
        }

        [Test]
        public void Clicking_BSD_Systems_Customize_ShouldGotoCustomize()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=bsd&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("Clicking_BSD_SNP_Customize_ShouldGotoCustomize.png", ScreenshotImageFormat.Png);
            //Click the BSD radio button
            RetryFindAndClick(By.Id("For-Work-radio-button"));
            Console.WriteLine($"Clicking Customize button");
            RetryFindAndClick(By.Id("customize-0"));
            Console.WriteLine($"Clicked on customize button ");
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("work/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);
        }

        [Test]
        public void Clicking_DHS_Systems_Customize_ShouldGotoCustomize()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("Clicking_BSD_SNP_Customize_ShouldGotoCustomize.png", ScreenshotImageFormat.Png);
            //Click the BSD radio button
            Console.WriteLine($"Clicking segment radio button");
            RetryFindAndClick(By.Id("For-Home-radio-button"));
            Console.WriteLine($"Clicking Customize button");
            RetryFindAndClick(By.Id("customize-0"));
            Console.WriteLine($"Clicked on customize button ");
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("shop/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);
        }

        [Ignore("Prod Deploy failed due to data issue: KOA Monkey deletes old documents")]
        [Test]
        public void SpecialEventToggleShouldRenderSpecialEventMarkerForDealProduct()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=TESTORDERCODE_IGNORE&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("SpecialEventToggleShouldRenderSpecialEventMarkerForDealProduct.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Look for an Element with deal icon tag");
            var specialeventmarker = RetryFind(By.ClassName("icon-small-deals"));
            Assert.NotNull(specialeventmarker);

        }

        [Test]
        public void Clicking_DHS_SNP_Customize_ShouldGotoCustomize()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[snp]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("Clicking_BSD_SNP_Customize_ShouldGotoCustomize.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Clicking segment radio button");
            RetryFindAndClick(By.Id("For-Home-radio-button"));
            Console.WriteLine($"Clicking Customize button");
            RetryFindAndClick(By.Id("cusomtize-0"));
            Console.WriteLine($"Clicked on customize button ");
            string landingUrl = _driver.Url;
            Console.WriteLine($"Landing page url is {landingUrl}");
            landingUrl = landingUrl.Substring(0, landingUrl.LastIndexOf("shop/") + 5);
            Assert.IsNotNull(landingUrl);
            //Assert.AreEqual(expectedServiceSelectionUrl, landingUrl);
        }
        [Test]
        public void Clicking_OfferDetails_ShouldShowModal()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("Clicking_OfferDetails_ShouldShowModal.png", ScreenshotImageFormat.Png);
            Console.WriteLine("Clicking First Offer");
            RetryFindAndClick(By.Id("offer-item-2-0"));
            var modal = RetryFind(By.Id("modal-card-2"));
            Assert.IsTrue(modal.Displayed);
            //click anywhere else
            modal.Click();
            Assert.IsFalse(modal.Displayed);
        }

        [Test]
        public void NewQuery_Should_ResetSortSelection()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("NewQuery_Should_ResetSortSelection.png", ScreenshotImageFormat.Png);
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("xps");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            var sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-High-to-Low"));
            Thread.Sleep(2000);
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Price High To Low", sortBy.Text);
            searchBox.Clear();
            searchBox.Click();
            searchBox.SendKeys("mouse");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
        }

        [Test]
        public void SelectingTwoSortOptions_DoesNotResetSort()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("SelectingTwoSortOptions_DoesNotResetSort.png", ScreenshotImageFormat.Png);
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("xps");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            var sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-High-to-Low"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Price High To Low", sortBy.Text);
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-Low-to-High"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Price Low To High", sortBy.Text);
        }

        [Test]
        public void SelectingSortOption_WithNewQuery_ShouldResetSortToRelevance()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("SelectingTwoSortOptions_DoesNotResetSort.png", ScreenshotImageFormat.Png);
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("xps");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            var sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-Low-to-High"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Price Low To High", sortBy.Text);
            searchBox.Clear();
            searchBox.Click();
            searchBox.SendKeys("mouse");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
        }

        [Test]
        public void DirectContentToggleTrueShouldRenderDirectContentCorrectly()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableDirectContentExperience=true&#q=order&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            Thread.Sleep(5000);
            //ss.SaveAsFile("DirectContentToggleTrueShouldRenderDirectContentCorrectly.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Look for an Element with Direct Content");
            var directcontent = RetryFind(By.ClassName("CoveoDirectContent"));
            Assert.NotNull(directcontent);
            var directcontentimage = RetryFind(By.Id("directContentImage"));
            Assert.NotNull(directcontentimage);
            var directcontentTitle = RetryFind(By.Id("directContentTitleText"));
            Assert.NotNull(directcontentTitle);
            var directcontentDescription = RetryFind(By.Id("directContentDescription"));
            Assert.NotNull(directcontentDescription);
            Assert.NotNull(directcontentDescription.Text);
            Assert.IsNotEmpty(directcontentDescription.Text);
        }

        [Test]
        public void DirectContentToggleTrueShouldNotRenderDirectContentWithOutDCKeywords()
        {
            //Search "drivers"
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableDirectContentExperience=true&#q=order&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Thread.Sleep(5000);
            Console.WriteLine($"Look for an Element with Direct Content");
            var directcontent = RetryFind(By.ClassName("CoveoDirectContent"));
            Assert.NotNull(directcontent);
            var directcontentimage = RetryFind(By.Id("directContentImage"));
            Assert.NotNull(directcontentimage);
            var directcontentTitle = RetryFind(By.Id("directContentTitleText"));
            Assert.NotNull(directcontentTitle);
            var directcontentDescription = RetryFind(By.Id("directContentDescription"));
            Assert.NotNull(directcontentDescription);
            Assert.NotNull(directcontentDescription.Text);
            Assert.IsNotEmpty(directcontentDescription.Text);
            //Search "xps"
            searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableDirectContentExperience=true&#q=xps&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Thread.Sleep(5000);
            Console.WriteLine($"Look for an Element with Direct Content");
            directcontent = RetryFind(By.ClassName("CoveoDirectContent"));
            Assert.IsEmpty(directcontent.Text);

        }

        [Test]
        public void DirectContentToggleFalseShouldNotRenderDirectContent()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableDirectContentExperience=false&#q=order&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("DirectContentToggleFalseShouldNotRenderDirectContent.png", ScreenshotImageFormat.Png);
            Console.WriteLine($"Look for an Element with Direct Content");
            var directcontent = RetryFind(By.ClassName("CoveoDirectContent"));
            Assert.Null(directcontent);
        }

        [Test]
        public void Product_Type_Facet_Labels_Vertical_Alignment_Correct()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=alienware&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for Desktops & Laptops facet label");
            IWebElement sysLabel = _driver.FindElement(By.CssSelector("span[title='Desktops & Laptops']"));
            Assert.NotNull(sysLabel);
            Console.WriteLine($"Look for Accessories facet label");
            IWebElement snpLabel = RetryFind(By.CssSelector("span[title='Accessories']"));
            Assert.NotNull(snpLabel);
            var sysLabelVerticalPos = sysLabel.Location.X;
            var snpLabelVerticalPos = snpLabel.Location.X;
            Assert.AreEqual(sysLabelVerticalPos, snpLabelVerticalPos);
        }

        [Test]
        public void Product_Type_Facet_Labels_Should_Have_Same_Height()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=alienware&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for Desktops & Laptops facet label");
            IWebElement sysLabel = _driver.FindElement(By.CssSelector("span[title='Desktops & Laptops']"));
            Assert.NotNull(sysLabel);
            Console.WriteLine($"Look for Accessories facet label");
            IWebElement snpLabel = RetryFind(By.CssSelector("span[title='Accessories']"));
            Assert.NotNull(snpLabel);
            var sysLabelHeight = sysLabel.Size.Height;
            var snpLabelHeight = snpLabel.Size.Height;
            Assert.AreEqual(sysLabelHeight, snpLabelHeight);
        }

        [Test]
        public void All_Facet_Check_Boxes_Vertical_Alignment_Correct()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=alienware&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for facet check boxes");
            var labels = _driver.FindElements(By.ClassName("coveo-facet-value-checkbox"));
            Assert.NotNull(labels);
            var pos = labels[0].Location.X;
            foreach (var e in labels)
            {
                Assert.AreEqual(pos, e.Location.X);
            }
        }

        [Test]
        public void All_Facet_Labels_Vertical_Alignment_Correct()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=alienware&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for facet labels");
            var labels = _driver.FindElements(By.ClassName("coveo-facet-value-caption"));
            Assert.NotNull(labels);
            var pos = labels[0].Location.X;
            foreach (var e in labels)
            {
                Assert.AreEqual(pos, e.Location.X);
            }
        }

        [Test]
        public void All_Facet_Labels_Have_Caption()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=alienware&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for facet labels");
            var labels = _driver.FindElements(By.ClassName("coveo-facet-value-caption"));
            Assert.NotNull(labels);
            var pos = labels[0].Location.X;
            foreach (var e in labels)
            {
                Assert.IsNotNull(e.Text);
                Assert.IsTrue(e.Text.Length > 0);
            }
        }

        [Test]
        public void All_Facet_Labels_Should_Have_Same_Height_PerLine()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=alienware&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for facet labels");
            var labels = _driver.FindElements(By.ClassName("coveo-facet-value-caption"));
            Assert.NotNull(labels);
            var pos = labels[0].Size.Height;
            foreach (var e in labels)
            {
                Assert.IsTrue(e.Size.Height % pos == 0 || pos % e.Size.Height == 0);
            }
        }

        [Test]
        public void All_Facet_Count_Value_Vertical_Alignment_Correct()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "&enableSpecialEvents=true#q=alienware&t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for facet Count");
            var labels = _driver.FindElements(By.ClassName("coveo-facet-value-count"));
            Assert.NotNull(labels);
            var pos = labels[0].Location.X + labels[0].Size.Width;
            foreach (var e in labels)
            {
                Assert.IsTrue(Math.Abs(pos - (e.Location.X + e.Size.Width)) < 2);
            }
        }

        [Ignore("Delivery Promise for SnP is rolledback as Delivery date data was wrong.")]
        [Test]
        public void Delivery_Promise_Should_Show_In_SNP_Card_View()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[snp]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for delivery promise");
            var labels = _driver.FindElements(By.ClassName("CoveoDeliveryPromise"));
            Assert.IsTrue(labels.Count > 0);
            Assert.NotNull(labels[0]);
            Assert.NotNull(labels[0].Text);
            Assert.NotNull(labels[0].Text.ToLower().Contains("get as soon as"));
        }

        [Ignore("Delivery Promise for SnP is rolledback as Delivery date data was wrong.")]
        [Test]
        public void Delivery_Promise_Should_Show_In_SNP_List_View()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#t=default&sort=relevancy&layout=list&@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[snp]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for delivery promise");
            var labels = _driver.FindElements(By.ClassName("CoveoDeliveryPromise"));
            Assert.IsTrue(labels.Count > 0);
            Assert.NotNull(labels[0]);
            Assert.NotNull(labels[0].Text);
            Assert.NotNull(labels[0].Text.ToLower().Contains("get as soon as"));
        }

        [Test]
        public void Delivery_Promise_Should_Show_In_OrderCode_Card_View()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#t=default&sort=relevancy&layout=card&@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for delivery promise");
            var labels = _driver.FindElements(By.ClassName("CoveoDeliveryPromise"));
            Assert.IsTrue(labels.Count > 0);
            Assert.NotNull(labels[0]);
            Assert.NotNull(labels[0].Text);
            Assert.NotNull(labels[0].Text.ToLower().Contains("get as soon as"));
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void Delivery_Promise_Should_Show_In_OrderCode_List_View()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#t=default&sort=relevancy&layout=list&@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            Console.WriteLine($"Look for delivery promise");
            var labels = _driver.FindElements(By.ClassName("CoveoDeliveryPromise"));
            Assert.IsTrue(labels.Count > 0);
            Assert.NotNull(labels[0]);
            Assert.NotNull(labels[0].Text);
            Assert.NotNull(labels[0].Text.ToLower().Contains("get as soon as"));
        }

        private bool RetryFindAndClick(By by, int waitMilliSeconds = 18000, int retry = 3)
        {
            bool result = false;
            int attempts = 0;
            while (attempts < retry)
            {
                attempts++;
                try
                {
                    var webDriverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(waitMilliSeconds));
                    webDriverWait.Until(d => d.FindElements(by))?.FirstOrDefault().Click();
                    result = true;
                    break;
                }
                catch
                {
                    if (waitMilliSeconds > 0)
                    {
                        Thread.Sleep(waitMilliSeconds);
                    }
                }
            }
            return result;
        }

        private IWebElement RetryFind(By by, int waitMilliSeconds = 18000, int retry = 3)
        {
            IWebElement result = null;
            int attempts = 0;
            while (attempts < retry)
            {
                attempts++;
                try
                {
                    var webDriverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(waitMilliSeconds));
                    result = webDriverWait.Until(d => d.FindElements(by))?.FirstOrDefault();
                    break;
                }
                catch
                {
                    if (waitMilliSeconds > 0)
                    {
                        Thread.Sleep(waitMilliSeconds);
                    }
                }
            }
            return result;
        }

        [TearDown]
        public void Dispose()
        {
            ExtractLogs(LogType.Browser);

            _driver.Quit();
        }

        public void ExtractLogs(string logType)
        {
            try
            {
                var logEntries = _driver.Manage().Logs.GetLog(logType);
                if (logEntries == null || !logEntries.Any())
                {
                    Console.WriteLine($"LogType {logType} not found.");
                }
                Console.WriteLine($"***************************************Starting Printing Logs {logType}********************************************");
                Console.WriteLine($"Timestamp           Level           Message");
                foreach (var entry in logEntries)
                {
                    Console.WriteLine($"{entry.Timestamp}   {entry.Level}   {entry.Message}");
                }
                Console.WriteLine($"***************************************Completed Printing Logs {logType}********************************************");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in ExtractLogs : {0}", ex.Message);
            }
        }
    }
}


