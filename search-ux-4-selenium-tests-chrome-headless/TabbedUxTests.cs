﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;

namespace SearchUx.Selenium
{
    public class TabbedUxTests : IDisposable
    {
        private IWebDriver _driver;
        private WebDriverWait _webDriverWait;

        public TabbedUxTests()
        {
            Console.WriteLine("TabbedUxTests Constructor ");
        }


        [SetUp]
        public void Init()
        {
            Console.WriteLine($"Is OsPlatform Linux - {RuntimeInformation.IsOSPlatform(OSPlatform.Linux)}");
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--headless");
            chromeOptions.AddArgument("--disable-gpu");
            chromeOptions.AddArgument("--no-sandbox");
            chromeOptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
            _driver = new ChromeDriver(chromeOptions);
            _webDriverWait = new WebDriverWait(_driver, TimeSpan.FromMilliseconds(10000));
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void ClickingSupportTab_ShouldShowSupportResults()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("ClickingSupportTab_ShouldShowSupportResults.png", ScreenshotImageFormat.Png);
            var supportTab = _driver.FindElement(By.Id("supportTab"));
            var productTab = _driver.FindElement(By.Id("default"));
            supportTab.Click();
            Assert.AreEqual("CoveoTab coveo-selected", supportTab.GetAttribute("class"));
            Assert.AreNotEqual("CoveoTab coveo-selected", productTab.GetAttribute("class"));
        }

        [Ignore("Known Issue")]
        [Test]
        public void CorrectDHSSegmentShouldShow_AfterSearchingInDHSSegment()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("CorrectDHSSegmentShouldShow_AfterSearchingInDHSSegment.png", ScreenshotImageFormat.Png);
            RetryFindAndClick(By.Id("For-Home-radio-button"));
            var onQuery = _driver.FindElement(By.Id("onquery"));
            Assert.AreEqual("for Home", onQuery.Text);
        }

        [Ignore("Known Issue: OnQuery summary string does not exist anymore so keeping this ignored")]
        [Test]
        public void CorrectBSDSegmentShouldShow_AfterSearchingInBSDSegment()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("CorrectBSDSegmentShouldShow_AfterSearchingInBSDSegment.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("For-Work-radio-button")).Click();
            var onQuery = _driver.FindElement(By.Id("onquery"));
            Assert.AreEqual("for Work", onQuery.Text);
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void WhenInSupportTab_SortOptionsShouldNotInclude_PriceSorting()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("WhenInSupportTab_SortOptionsShouldNotInclude_PriceSorting.png", ScreenshotImageFormat.Png);

            _driver.FindElement(By.Id("supportTab")).Click();
            _driver.FindElement(By.Id("SortDropdowndropdownMenuButton")).Click();
            var sortList = _driver.FindElements(By.ClassName("dropdown-item"));

            Assert.IsFalse(sortList.Any(x => x.Text.Contains("Price")));
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void WhenInShoppingTab_SortOptionsShouldNotInclude_DateSorting()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("WhenInShoppingTab_SortOptionsShouldNotInclude_DateSorting.png", ScreenshotImageFormat.Png);

            _driver.FindElement(By.Id("default")).Click();
            _driver.FindElement(By.Id("SortDropdowndropdownMenuButton")).Click();
            var sortList = _driver.FindElements(By.ClassName("dropdown-item"));

            Assert.IsFalse(sortList.Any(x => x.Text.Contains("Date")));
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void SwitchingTabs_ShouldKeepQueryTheSame()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("SwitchingTabs_ShouldKeepQueryTheSame.png", ScreenshotImageFormat.Png);
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("xps");
            Thread.Sleep(5000);
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            Thread.Sleep(5000);
            var querySummaries = _driver.FindElements(By.ClassName("coveo-highlight"));
            Assert.IsTrue(querySummaries.Any(x => x.Text.Contains("xps")), "checking for xps in the first search");
            _driver.FindElement(By.Id("supportTab")).Click();
            Thread.Sleep(5000);
            querySummaries = _driver.FindElements(By.ClassName("coveo-highlight"));
            Assert.IsTrue(querySummaries.Any(x => x.Text.Contains("xps")), "checking for xps in second search after tab switch");
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void SwitchingTabs_ShouldResetSort()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("SwitchingTabs_ShouldKeepQueryTheSame.png", ScreenshotImageFormat.Png);
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("xps");
            Thread.Sleep(5000);
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            Thread.Sleep(5000);
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-Low-to-High"));
            var sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Price Low To High", sortBy.Text);
            _driver.FindElement(By.Id("supportTab")).Click();
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
        }

        //[Ignore("Confirm edge case: Still a bug, sort is reset to relevance. Check if this feature is required before fixing")]
        [Test, Ignore("Temp ignoring to check if one works")]
        public void SelectingSortOption_WithNewQueryInSupport_DoesNotResetSort()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("SelectingTwoSortOptions_DoesNotResetSort.png", ScreenshotImageFormat.Png);
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            _driver.FindElement(By.Id("supportTab")).Click();
            searchBox.Click();
            searchBox.SendKeys("driver");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            var sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Date:-Newest-to-Oldest"));
            Assert.AreEqual("Date: Newest To Oldest", sortBy.Text);
            searchBox.Clear();
            searchBox.Click();
            searchBox.SendKeys("manual");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
        }

        [Ignore("New issue on sort: already in Pivotal Tracker: #159410465. This bug needs to be fixed before running this test")]
        [Test]
        public void SwitchingSortInShopping_ShouldShowCorrectLabel_WhenSupportExperienceEnabled()
        {
            var searchHomePageURl = Environment.GetEnvironmentVariable("UI_URL");
            searchHomePageURl = searchHomePageURl + "#@dpsalessegment:radioGroup=dhs&f:@dpproducttype=[system]";
            Console.WriteLine($"Navigating to {searchHomePageURl}");
            _driver.Navigate().GoToUrl(searchHomePageURl);
            Console.WriteLine($"Successfully navigated to {searchHomePageURl}");
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            Console.WriteLine($"Successfully taken screenshot");
            //ss.SaveAsFile("SwitchingSortInShopping_ShouldShowCorrectLabel_WhenSupportExperienceEnabled.png", ScreenshotImageFormat.Png);
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            //_driver.FindElement(By.Id("supportTab")).Click();
            //searchBox.Click();
            searchBox.SendKeys("xps");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            var sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Relevance", sortBy.Text);
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-High-to-Low"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Price High To Low", sortBy.Text);
            searchBox.Clear();
            searchBox.Click();
            searchBox.SendKeys("laptop");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-High-to-Low"));
            RetryFindAndClick(By.ClassName("dropdown-wrapper"));
            RetryFindAndClick(By.Id("dropdown-item-Price-High-to-Low"));
            sortBy = _driver.FindElement(By.ClassName("dropdown-selected-value"));
            Assert.AreEqual("Price High To Low", sortBy.Text);
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void ClickingTab_ShouldDeclareIntent_AndStickAfterRefresh_OnNewQuery()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("SearchingForSupportTerms_ShouldSwitchAutomaticallyToSupport_AndViceVersa.png", ScreenshotImageFormat.Png);
            _driver.FindElement(By.Id("supportTab")).Click();
            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("xps");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            Thread.Sleep(5000);
            var supportIcon = _driver.FindElement(By.ClassName("CoveoSupportIcon"));
            Assert.IsTrue(supportIcon.Displayed);
        }

        [Test, Ignore("Temp ignoring to check if one works")]
        public void SearchingForSupportTerms_ShouldSwitchAutomaticallyToSupport_AndViceVersa()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("SearchingForSupportTerms_ShouldSwitchAutomaticallyToSupport_AndViceVersa.png", ScreenshotImageFormat.Png);

            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("driver");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            Thread.Sleep(5000);
            var supportIcon = _driver.FindElement(By.ClassName("CoveoSupportIcon"));
            Assert.IsTrue(supportIcon.Displayed);

            searchBox.Clear();
            searchBox.Click();
            searchBox.SendKeys("xps");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            Thread.Sleep(5000);
            var productIcon = _driver.FindElement(By.ClassName("ProductImage"));
            Assert.IsTrue(productIcon.Displayed);
        }

        /*
         *  check for class CoveoResultList
         *  check attribute: data-layout
         * 
         */

        [Test, Ignore("Temp ignoring to check if one works")]
        public void SwitchingFromSupportToShopping_AfterComponentClick_ShouldSwitchShoppingToCardView()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("SwitchingFromSupportToShopping_AfterComponentClick_ShouldSwitchShoppingToCardView.png", ScreenshotImageFormat.Png);

            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            searchBox.Click();
            searchBox.SendKeys("driver");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            Thread.Sleep(5000);
            var supportIcon = _driver.FindElement(By.ClassName("CoveoSupportIcon"));
            Assert.IsTrue(supportIcon.Displayed);
            Thread.Sleep(5000);
            RetryFindAndClick(By.ClassName("coveo-facet-value-label-wrapper"));

            searchBox.Clear();
            searchBox.Click();
            searchBox.SendKeys("xps");
            RetryFindAndClick(By.ClassName("CoveoSearchButton"));
            Thread.Sleep(5000);
            var productIcon = _driver.FindElement(By.ClassName("ProductImage"));
            Assert.IsTrue(productIcon.Displayed);

            var resultView = _driver.FindElements(By.ClassName("CoveoResultList")).FirstOrDefault(x => !x.GetAttribute("class").Contains("coveo-hidden"));
            Assert.AreEqual("card", resultView.GetAttribute("data-layout"));
        }

        // [Ignore("Ignoring to push to prod")]
        [Test, Ignore("Temp ignoring to check if one works")]
        public void WhenCustomerEntersSearchInSupportTab_SwitchingtoShopping_ShouldShowResultsInCardView()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}#q=drivers";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("SwitchingFromSupportToShopping_AfterComponentClick_ShouldSwitchShoppingToCardView.png", ScreenshotImageFormat.Png);

            var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            Thread.Sleep(5000);
            var supportIcon = _driver.FindElement(By.ClassName("CoveoSupportIcon"));
            Assert.IsTrue(supportIcon.Displayed, "In support the first time");
            Thread.Sleep(5000);
            var resultView = _driver.FindElements(By.ClassName("CoveoResultList")).FirstOrDefault(x => !x.GetAttribute("class").Contains("coveo-hidden"));
            Assert.AreEqual("list", resultView.GetAttribute("data-layout"));
            Thread.Sleep(5000);
            RetryFindAndClick(By.ClassName("coveo-facet-value-label-wrapper"));
            Thread.Sleep(5000);
            RetryFindAndClick(By.Id("default"));
            Thread.Sleep(5000);

            var productIcon = _driver.FindElement(By.ClassName("product-picture"));
            // Assert.IsTrue(productIcon.Displayed, "In shopping");

            resultView = _driver.FindElements(By.ClassName("CoveoResultList")).FirstOrDefault(x => !x.GetAttribute("class").Contains("coveo-hidden"));
            Assert.AreEqual("card", resultView.GetAttribute("data-layout"), "In shopping with card view");
        }

        //[Ignore("Need to fix OnQuery bug for this")]
        [Test, Ignore("Temp ignoring to check if one works")]
        public void WhenCustomerEntersSearchInSupportTab_SwitchingtoShopping_ShouldNotShowSupportData()
        {
            var searchPageUrlWithTabs = $"{Environment.GetEnvironmentVariable("UI_URL")}#q=driver&t=supportTab&sort=relevancy&@dpsalessegment:radioGroup=dhs";
            Console.WriteLine($"Navigating to {searchPageUrlWithTabs}");
            _driver.Navigate().GoToUrl(searchPageUrlWithTabs);
            //Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
            //ss.SaveAsFile("SwitchingFromSupportToShopping_AfterComponentClick_ShouldSwitchShoppingToCardView.png", ScreenshotImageFormat.Png);
            //var searchBox = _driver.FindElement(By.CssSelector("#SearchBoxHeader > div > div > div.magic-box-input > input"));
            Thread.Sleep(5000);
            var supportIcon = _driver.FindElement(By.ClassName("CoveoSupportIcon"));
            Assert.IsTrue(supportIcon.Displayed);
            Thread.Sleep(5000);
            RetryFindAndClick(By.Id("default"));
            Thread.Sleep(5000);
            var priceFirstel = _driver.FindElement(By.Id("price-0"));
            Regex digitsOnly = new Regex(@"[^\d\.]");
            string priceFirst = digitsOnly.Replace(priceFirstel.Text, "");
            Assert.NotNull(priceFirst);
        }

        private bool RetryFindAndClick(By by, int waitMilliSeconds = 18000, int retry = 3)
        {
            bool result = false;
            int attempts = 0;
            while (attempts < retry)
            {
                attempts++;
                try
                {
                    var webDriverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(waitMilliSeconds));
                    webDriverWait.Until(d => d.FindElements(by))?.FirstOrDefault().Click();
                    result = true;
                    break;
                }
                catch
                {
                    if (waitMilliSeconds > 0)
                    {
                        Thread.Sleep(waitMilliSeconds);
                    }
                }
            }
            return result;
        }

        [TearDown]
        public void Dispose()
        {
            ExtractLogs(LogType.Browser);

            _driver.Quit();
        }

        public void ExtractLogs(string logType)
        {
            try
            {
                var logEntries = _driver.Manage().Logs.GetLog(logType);
                if (logEntries == null || !logEntries.Any())
                {
                    Console.WriteLine($"LogType {logType} not found.");
                }
                Console.WriteLine($"***************************************Starting Printing Logs {logType}********************************************");
                Console.WriteLine($"Timestamp           Level           Message");
                foreach (var entry in logEntries)
                {
                    Console.WriteLine($"{entry.Timestamp}   {entry.Level}   {entry.Message}");
                }
                Console.WriteLine($"***************************************Completed Printing Logs {logType}********************************************");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in ExtractLogs : {0}", ex.Message);
            }
        }
    }
}
